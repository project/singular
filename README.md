# Singular

Drupal 8 base theme project based in formata and bootstrap

## Contents
* Drupal's 8 TWIG based 
* SASS partials structure.
* Singularity Grid System for responsive grids. 
* Breakpoint media queries in Sass
* Typecsset for vertical rhythm typography
* Code syntax highlighting with Rainbow
* Google Fonts Merriweather Serif and Sans-serif families
* Font Awesome Icons

## Installation

Place folder under /sitename/themes/ and enable theme in /admin/appearance

## Usage 
* [Singularity](https://github.com/at-import/Singularity/wiki)
* [Breakpoint](https://github.com/at-import/breakpoint/wiki)
* [Typecsset](https://github.com/csswizardry/typecsset)
* [Font Awesome Examples](https://fortawesome.github.io/Font-Awesome/examples/)

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D


## Road Map

* Ability to create [subthemes](https://www.drupal.org/node/225125) without hacking main theme
* Add [Require.js](http://requirejs.org/docs/start.html)
* [Critical CSS](https://github.com/filamentgroup/criticalCSS)
* Add [Pattern Lab support](https://github.com/pattern-lab/patternengine-php-twig) as integration with prototype and styleguides.
* Add [Mustache](https://github.com/mustache/mustache.github.com) generated content for Pattern Lab templates

## Credits

[https://www.appnovation.com/blog/creating-drupal-8-theme-sasssingularity-breakpoint](https://www.appnovation.com/blog/creating-drupal-8-theme-sasssingularity-breakpoint)
[https://www.drupal.org/project/bootstrap](https://www.drupal.org/project/bootstrap)

## License
[Creative Commons Attribution-ShareAlike license 2.0.](http://creativecommons.org/licenses/by-sa/2.0/)

